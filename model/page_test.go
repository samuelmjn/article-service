package model

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

type PageServiceSuccess struct{}

func (ps PageServiceSuccess) IsPageLimited() bool {
	return false
}

func (ps PageServiceSuccess) CheckSlugIsExist(s string) bool {
	return false
}

func (ps PageServiceSuccess) GetAllPages() []Page {
	return []Page{
		Page{"lorem-ipsum", "Lorem Ipsum", "ini lorem ipsum", time.Now(), PagePublished},
		Page{"lorem-ipsum2", "Lorem Ipsum", "ini lorem ipsum", time.Now(), PageNotPublished},
	}
}

type PageServiceError struct{}

func (ps PageServiceError) IsPageLimited() bool {
	return true
}

func (ps PageServiceError) CheckSlugIsExist(s string) bool {
	return true
}

func (ps PageServiceError) GetAllPages() []Page {
	return []Page{}
}

func TestCreatePage(t *testing.T) {
	t.Run("Correct", func(t *testing.T) {
		// given
		name := "About Us"
		slug := "about-us"
		body := "This is about us page"
		ps := PageServiceSuccess{}

		// when
		got, err := CreatePage(ps, name, slug, body)

		// then
		assert.Equal(t, name, got.Name)
		assert.Equal(t, slug, got.Slug)
		assert.Equal(t, body, got.Body)
		assert.NotEmpty(t, got.CreatedAt)
		assert.Equal(t, PageNotPublished, got.Status)
		assert.Nil(t, err)
	})

	t.Run("Exceed the limit", func(t *testing.T) {
		// given
		name := "About Us"
		slug := "about-us"
		body := "This is about us page"
		ps := PageServiceError{}

		// when
		_, err := CreatePage(ps, name, slug, body)

		// then
		assert.Error(t, err)
	})
}

func TestCreatePageWithSlug(t *testing.T) {
	t.Run("Newly slug", func(t *testing.T) {
		// GIVEN
		name := "Contact us"
		slug := "Contact-us"
		body := "Email: boo@moo.com"
		ps := PageServiceSuccess{}

		// WHEN
		val, err := CreatePage(ps, name, slug, body)

		// THEN
		assert.Empty(t, err)
		assert.NotEmpty(t, val.Slug)
	})

	t.Run("Empty Slug", func(t *testing.T) {
		// GIVEN
		name := "Contact us"
		slug := ""
		body := "Email: boo@moo.com"
		ps := PageServiceSuccess{}

		// WHEN
		val, err := CreatePage(ps, name, slug, body)

		// THEN
		assert.Empty(t, err)
		assert.NotEmpty(t, val.Slug)
	})

	t.Run("Already Exists Slug", func(t *testing.T) {
		// GIVEN
		name := "Contact us"
		slug := "Contact-us"
		body := "Email: boo@moo.com"
		ps := PageServiceError{}

		// WHEN
		_, err := CreatePage(ps, name, slug, body)

		// THEN
		assert.Error(t, err)
	})
}

func TestCanFindPageBySlug(t *testing.T) {
	// GIVEN
	slug := "lorem-ipsum"
	ps := PageServiceSuccess{}
	//	WHEN
	page, _ := FindPageBySlug(ps, slug)
	// THEN
	assert.Equal(t, slug, page.Slug)
}

func TestCantFindPageBySlug(t *testing.T) {
	// GIVEN
	slug := "lorem-ipsum2"
	ps := PageServiceSuccess{}
	//	WHEN
	_, err := FindPageBySlug(ps, slug)
	// THEN
	assert.Error(t, err, "Page not found!")
}
