package repository

import (
	"gitlab.com/fannyhasbi/article-service/model"
)

type ArticleStorage struct {
	ArticleMap map[string]model.Article
}

func CreateArticleStorage() *ArticleStorage {
	return &ArticleStorage{
		ArticleMap: make(map[string]model.Article),
	}
}

func (s *ArticleStorage) IsSlugExist(slug string) bool {
	for _, article := range s.ArticleMap {
		if article.Slug == slug {
			return true
		}
	}
	return false
}

func (s *ArticleStorage) SaveArticle(article *model.Article) string {
	s.ArticleMap[article.Slug] = *article
	return article.Slug
}
